package com.example.topipenttila.myapplication;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by topipenttila on 29/09/15.
 * Entry class has the information about entries (articles)
 */
public class Entry implements Serializable {

    private String title;
    private String description;
    private long pubDate;
    private String link;
    private String pubDateString;
    private String channelTitle;

    public Entry(String title, String description, long pubDate, String link, String pubDateString, String channelTitle) {
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.link = link;
        this.pubDateString = pubDateString;
        this.channelTitle = channelTitle;

    }

    public String getLink() {
        return this.link;
    }

    public String toString() {
        return this.pubDateString + "\n\n" + this.title + "\n\n" + this.channelTitle;
    }

    public long getPubDate() {
        return this.pubDate;
    }

    public String getEntryString() {
        String str = title + description + pubDateString + link;
        return str;
    }

    public String getTitle() {
        return this.title;
    }


}