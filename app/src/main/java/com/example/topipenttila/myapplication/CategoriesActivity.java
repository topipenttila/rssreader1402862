package com.example.topipenttila.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.net.MalformedURLException;

import static android.R.layout.simple_list_item_1;

/**
 * Created by topipenttila on 03/10/15.
 * CategoriesActivity shows the categories
 */
public class CategoriesActivity extends AppCompatActivity {
    private FeedReader reader;
    private ListView feedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_view);
        feedView = (ListView) findViewById(R.id.feedView);
        try {
            reader = new FeedReader();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayAdapter adapter = new ArrayAdapter(this,
                simple_list_item_1, android.R.id.text1, reader.getCategories());
        feedView.setAdapter(adapter);

        feedView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                //OnItemClick opens MainActivity and provides position information with which the right adapter is chosen
                try {
                    Intent intent = new Intent(CategoriesActivity.this, MainActivity.class);
                    intent.putExtra("position", position);
                    startActivity(intent);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
                /*if (id == R.id.action_settings) {
                    return true;
                }*/

        return super.onOptionsItemSelected(item);
    }

    public void equalClicked(View v) throws IOException {

        runOnUiThread(new Runnable() {
            public void run() {

                ((BaseAdapter) feedView.getAdapter()).notifyDataSetChanged();

            }
        });

    }

}
