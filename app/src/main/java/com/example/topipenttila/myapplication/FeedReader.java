package com.example.topipenttila.myapplication;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by topipenttila on 29/09/15.
 * FeedReader parses the feeds
 */
public class FeedReader extends Activity implements Runnable {
    User user = User.getInstance();
    ArrayList<String> categories;
    String title = "";
    String description = "";
    String pubDateString = "";
    String channelTitle = "";
    String link = "";
    String category = "";
    int position;
    long timeInMinutesSinceEpoch;
    OutputStreamWriter out;

    ArrayList<URL> urls;

    public FeedReader() throws IOException {
        this.position = 0;

        categories = new ArrayList<>();
        categories.add("All");
        categories.add("Uutiset");
        categories.add("Politiikka");
        categories.add("Kotimaa");
        saveUrls();
        restoreUrls();
        user.addUrls();
        urls = user.getUrls();

    }

    @Override
    public void run() {
        user.emptyAll();

        for (URL url : urls) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(new BufferedReader(new InputStreamReader(urlConnection.getInputStream())));

                boolean insideItem = false;

                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {

                    if (eventType == XmlPullParser.START_DOCUMENT) {

                    } else if (eventType == XmlPullParser.START_TAG) {
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        } else if (xpp.getName().equalsIgnoreCase("title")) {
                            if (insideItem) {
                                title = xpp.nextText(); //extract the title
                            } else {
                                channelTitle = xpp.nextText();

                            }
                        } else if (xpp.getName().equalsIgnoreCase("link")) {
                            if (insideItem) {
                                link = xpp.nextText(); //extract the link
                            }
                        } else if (xpp.getName().equalsIgnoreCase("description")) {
                            if (insideItem) {
                                description = xpp.nextText(); //extract the description
                            }
                        } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                            if (insideItem) {
                                pubDateString = xpp.nextText(); //extract the publish date
                                //Parses the date to a format which can be sorted
                                SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
                                Date date = sdf.parse(pubDateString);
                                long timeInMillisSinceEpoch = date.getTime();
                                timeInMinutesSinceEpoch = timeInMillisSinceEpoch / (60 * 1000);
                            }
                        } else if (xpp.getName().equalsIgnoreCase("category")) {
                            if (insideItem) {
                                category = xpp.nextText(); //extract the category
                            }

                        }
                    } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        Entry entry = new Entry(title, description, timeInMinutesSinceEpoch, link, pubDateString, channelTitle);
                        user.addToAll(entry);
                        if (this.category.toLowerCase().contains("politiikka")) {
                            user.addToPolitics(entry);
                        }
                        if (this.category.toLowerCase().contains("uutiset")) {
                            user.addToNews(entry);
                        }
                        if (this.category.toLowerCase().contains("kotimaa")) {
                            user.addToDomestic(entry);
                        }

                        insideItem = false;

                    } else if (eventType == XmlPullParser.END_TAG) {

                    } else if (eventType == XmlPullParser.TEXT) {

                    }

                    eventType = xpp.next();

                }
            } catch (Exception e) {
                System.out.println(e);

            }
        }

    }


    public String getTile() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String getPubDate() {
        return this.pubDateString;
    }

    public ArrayList<String> getCategories() {
        return this.categories;
    }


    public ArrayList<URL> getUrls() {
        return urls;
    }

    //Unused, potential use in the future
    private void saveUrls() {
        try {
            FileOutputStream out = openFileOutput("urls.ser", Context.MODE_PRIVATE);
            ObjectOutputStream obout = new ObjectOutputStream(out);
            obout.writeObject(urls);
            obout.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not open file");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error writing into file");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Unused, potential use in the future
    private void restoreUrls() {
        try {
            FileInputStream in = new FileInputStream("urls.ser");
            ObjectInputStream obin = new ObjectInputStream(in);
            urls = (ArrayList) obin.readObject();
            obin.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not open file");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Error reading object");
            e.printStackTrace();
        }
    }
}
