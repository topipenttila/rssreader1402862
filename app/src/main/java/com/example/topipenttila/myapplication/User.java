package com.example.topipenttila.myapplication;

import android.graphics.Movie;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by topipenttila on 01/10/15.
 * User Singleton class has all the arraylists of articles and feed URLs as well as getters and setters for those
 */

public class User {
    private static final User INSTANCE = new User();
    private ArrayList<Entry> allEntriesList;
    private ArrayList<Entry> customList;
    private ArrayList<Entry> newsList;
    private ArrayList<Entry> politicsList;
    private ArrayList<Entry> domesticList;

    private ArrayList<Entry> favoritesList;

    private ArrayList<Entry> readList;

    private ArrayList<String> categories;

    private ArrayList<URL> urls;

    private boolean urlsAdded;


    private ArrayList<FeedUpdateObserver> observers;

    private User() {
        this.allEntriesList = new ArrayList<>();
        this.customList = new ArrayList<>();
        this.newsList = new ArrayList<>();
        this.politicsList = new ArrayList<>();
        this.domesticList = new ArrayList<>();

        this.favoritesList = new ArrayList<>();

        this.readList = new ArrayList<>();

        this.observers = new ArrayList<>();

        urls = new ArrayList<>();

        this.urlsAdded = false;


    }

    public void addUrls() throws MalformedURLException {
        if (!this.urlsAdded) {
            urls.add(new URL("http://yle.fi/uutiset/rss/paauutiset.rss"));
            urls.add(new URL("http://yle.fi/uutiset/rss/uutiset.rss?osasto=politiikka"));
            urls.add(new URL("http://yle.fi/uutiset/rss/uutiset.rss?osasto=kulttuuri"));
            urls.add(new URL("http://yle.fi/uutiset/rss/uutiset.rss?osasto=viihde"));
            urls.add(new URL("http://yle.fi/uutiset/rss/uutiset.rss?osasto=tiede"));
            this.urlsAdded = true;
        }
    }

    public void registerObserver(FeedUpdateObserver o) {
        observers.add(o);
    }

    public void deregisterObserver(FeedUpdateObserver o) {
        observers.remove(o);
    }

    public void notifyObservers() {
        for (FeedUpdateObserver o : observers)
            o.feedUpdated();
    }

    public void addToAll(Entry e) {
        allEntriesList.add(e);
    }

    public void removeFromAll(Entry e) {
        allEntriesList.remove(e);
    }

    public ArrayList getAllEntries() {

        return allEntriesList;
    }

    public static User getInstance() {
        return INSTANCE;
    }

    public void addToCustom(Entry e) {
        this.customList.add(e);
    }

    public void addToDomestic(Entry e) {
        this.domesticList.add(e);
    }

    public void addToNews(Entry e) {
        this.newsList.add(e);
    }

    public void addToPolitics(Entry e) {
        this.politicsList.add(e);
    }

    public void addToFavorites(Entry e) {
        this.favoritesList.add(e);
    }

    public void removeFavorite(Entry e) {
        this.favoritesList.remove(e);
    }

    public void addToRead(Entry e) {
        this.readList.add(e);
    }

    public ArrayList getCustom() {
        return this.customList;
    }

    public ArrayList getDomestic() {
        return this.domesticList;
    }

    public ArrayList getNews() {
        return this.newsList;
    }

    public ArrayList getPolitics() {
        return this.politicsList;
    }

    public ArrayList getFavorites() {
        return this.favoritesList;
    }

    public ArrayList getRead() {
        return this.readList;
    }

    public void hasText(String text) {
        customList.clear();
        for (Entry e : this.allEntriesList) {
            if (e.getEntryString().toLowerCase().contains(text.toLowerCase())) {
                customList.add(e);
            }
        }
    }

    public void emptyAll() {
        this.allEntriesList.clear();
        this.domesticList.clear();
        this.newsList.clear();
        this.politicsList.clear();
    }

    public ArrayList getUrls() {
        return this.urls;
    }

    public void addUrl(URL url) {
        this.urls.add(url);
    }

    public void removeUrl(int position) {
        this.urls.remove(position);
    }

    //Sorts the list by publish date
    public void sortList(ArrayList list) {
        Collections.sort(list, new Comparator<Entry>() {
            @Override
            public int compare(Entry rss1, Entry rss2) {
                if (rss1.getPubDate() < rss2.getPubDate())
                    return 1;
                if (rss1.getPubDate() > rss2.getPubDate())
                    return -1;
                return 0;
            }
        });
    }
}
