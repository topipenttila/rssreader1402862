package com.example.topipenttila.myapplication;

/**
 * Created by topipenttila on 05/10/15.
 * Observer interface for feed updates
 */
public interface FeedUpdateObserver {
    public void feedUpdated();
}
