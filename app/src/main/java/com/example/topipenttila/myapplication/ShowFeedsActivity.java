package com.example.topipenttila.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;

import static android.R.layout.simple_list_item_1;

/**
 * Created by topipenttila on 10/10/15.
 * ShowFeedsActivity shows the feed URLs
 */
public class ShowFeedsActivity extends AppCompatActivity {
    private FeedReader reader;
    private ListView urlView;
    User user = User.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remove_feed);
        urlView = (ListView) findViewById(R.id.feedListView1);

        try {
            reader = new FeedReader();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread t = new Thread(reader);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayAdapter adapter = new ArrayAdapter(this,
                simple_list_item_1, android.R.id.text1, user.getUrls());
        urlView.setAdapter(adapter);
        //OnItemClick removes URL from the list
        urlView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Context context = getApplicationContext();
                CharSequence text = "Feed URL " + reader.getUrls().get(position) + " succesfully removed";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                user.removeUrl(position);
                finish();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
                /*if (id == R.id.action_settings) {
                    return true;
                }*/

        return super.onOptionsItemSelected(item);

    }


}

