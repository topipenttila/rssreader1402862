package com.example.topipenttila.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by topipenttila on 10/10/15.
 * FavoritesActivity shows the favourited articles for the user
 */
public class FavoritesActivity extends AppCompatActivity {

    User user = User.getInstance();
    private ListView feedListView1;
    FeedReader reader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Oncreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        try {
            reader = new FeedReader();
        } catch (IOException e) {
            e.printStackTrace();
        }

        feedListView1 = (ListView) findViewById(R.id.feedListView1);

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, user.getFavorites());
        feedListView1.setAdapter(adapter);

        feedListView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //onItemClick opens a web view
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Entry e = (Entry) feedListView1.getItemAtPosition(position);
                user.addToRead(e);
                user.removeFromAll(e);

                String url = e.getLink();
                try {
                    Intent intent = new Intent(FavoritesActivity.this, ArticleActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });

        feedListView1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            //onItemLongClick removes article from favorites
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                           long id) {

                Entry e = (Entry) feedListView1.getItemAtPosition(position);

                user.removeFavorite(e);
                Context context = getApplicationContext();
                CharSequence text = "Feed " + e.getTitle() + " removed from favorites";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        View view;
        AlertDialog.Builder alertBuilder;
        final EditText userInput;
        int option = item.getItemId();
        Intent intent;
        switch (option) {
            //User can add feeds by URL, uses alertBuilder
            case R.id.action_addFeed:
                view = LayoutInflater.from(FavoritesActivity.this).inflate(R.layout.add_feed, null);

                alertBuilder = new AlertDialog.Builder(FavoritesActivity.this);
                alertBuilder.setView(view);
                userInput = (EditText) view.findViewById(R.id.userInput);


                alertBuilder.setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String u = userInput.getText().toString().toLowerCase();
                                try {
                                    URL url = new URL(u);
                                    user.addUrl(url);
                                    Context context = getApplicationContext();
                                    CharSequence text = "Feed URL " + url + " succesfully added";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                } catch (Exception e) {
                                    Context context = getApplicationContext();
                                    CharSequence text = "Not a valid URL";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                }

                            }
                        });
                Dialog dialog = alertBuilder.create();
                dialog.show();

                break;
            //Remove feed starts a new activity
            case R.id.action_removeFeed:
                intent = new Intent(FavoritesActivity.this, ShowFeedsActivity.class);
                startActivity(intent);

                break;
            //Favorites starts a new activity where the favorited articles are shown
            case R.id.action_favorites:
                intent = new Intent(FavoritesActivity.this, FavoritesActivity.class);
                startActivity(intent);
                break;
            //Read starts a new activity where the clicked articles are shown
            case R.id.action_read:
                intent = new Intent(FavoritesActivity.this, ReadActivity.class);
                startActivity(intent);
                break;
            //Help starts a new activity
            case R.id.action_help:
                intent = new Intent(FavoritesActivity.this, HelpActivity.class);
                startActivity(intent);
                break;

            //Search uses alertBuilder
            case R.id.action_search:
                view = LayoutInflater.from(FavoritesActivity.this).inflate(R.layout.search, null);

                alertBuilder = new AlertDialog.Builder(FavoritesActivity.this);
                alertBuilder.setView(view);
                userInput = (EditText) view.findViewById(R.id.userInput);


                alertBuilder.setCancelable(true)
                        .setPositiveButton("Go", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Search starts a new MainActivity with custom made list for adapter
                                user.hasText(userInput.getText().toString());
                                int position = 4;
                                Intent intent = new Intent(FavoritesActivity.this, MainActivity.class);
                                intent.putExtra("position", position);
                                startActivity(intent);
                            }
                        });
                Dialog dialog1 = alertBuilder.create();
                dialog1.show();

                break;
            //Home button goes back to CategoriesActivity
            case R.id.action_home:
                try {
                    finish();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                break;
            //Refresh gets new articles
            case R.id.action_refresh:
                Thread t = new Thread(reader);
                t.start();

                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                user.notifyObservers();
                break;
        }

        return false;


    }
}
