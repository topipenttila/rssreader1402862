package com.example.topipenttila.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements FeedUpdateObserver {
    private FeedReader reader;
    private ListView feedListView;
    User user = User.getInstance();
    Thread t;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        feedListView = (ListView) findViewById(R.id.feedListView);
        user.registerObserver(this);

        //MainActivity is registered as an observer for user
        User.getInstance().registerObserver(this);

        try {
            reader = new FeedReader();
        } catch (Exception e) {
            e.printStackTrace();
        }
        t = new Thread(reader);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Adapter gets a position from CategoriesActivity depending on which category is selected

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, user.getAllEntries());
        if (position == 1) {
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, user.getNews());

        }
        if (position == 2) {
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, user.getPolitics());

        }
        if (position == 3) {
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, user.getDomestic());

        }
        if (position == 4) {
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, user.getCustom());

        }
        if (position == 5) {
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, user.getFavorites());

        }
        feedListView.setAdapter(adapter);

        feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Entry e = (Entry) feedListView.getItemAtPosition(position);
                //The clicked article is marked at read
                user.addToRead(e);
                user.removeFromAll(e);

                String url = e.getLink();
                try {
                    Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });
        //User can add an article to favourites by LongClick
        feedListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                           long id) {

                Entry e = (Entry) feedListView.getItemAtPosition(position);

                user.addToFavorites(e);
                Context context = getApplicationContext();
                CharSequence text = "Feed " + e.getTitle() + " added to favourites";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                return true;
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // mDrawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        View view;
        AlertDialog.Builder alertBuilder;
        final EditText userInput;
        int option = item.getItemId();
        Intent intent;
        switch (option) {
            //User can add feeds by URL, uses alertBuilder
            case R.id.action_addFeed:
                view = LayoutInflater.from(MainActivity.this).inflate(R.layout.add_feed, null);

                alertBuilder = new AlertDialog.Builder(MainActivity.this);
                alertBuilder.setView(view);
                userInput = (EditText) view.findViewById(R.id.userInput);


                alertBuilder.setCancelable(true)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String u = userInput.getText().toString().toLowerCase();
                                try {
                                    URL url = new URL(u);
                                    user.addUrl(url);
                                    Context context = getApplicationContext();
                                    CharSequence text = "Feed URL " + url + " succesfully added";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                } catch (Exception e) {
                                    Context context = getApplicationContext();
                                    CharSequence text = "Not a valid URL";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                }

                            }
                        });
                Dialog dialog = alertBuilder.create();
                dialog.show();

                break;
            //Remove feed starts a new activity
            case R.id.action_removeFeed:
                intent = new Intent(MainActivity.this, ShowFeedsActivity.class);
                startActivity(intent);

                break;
            //Favorites starts a new activity where the favorited articles are shown
            case R.id.action_favorites:
                intent = new Intent(MainActivity.this, FavoritesActivity.class);
                startActivity(intent);
                break;
            //Read starts a new activity where the clicked articles are shown
            case R.id.action_read:
                intent = new Intent(MainActivity.this, ReadActivity.class);
                startActivity(intent);
                break;
            //Help starts a new activity
            case R.id.action_help:
                intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
                break;

            //Search uses alertBuilder
            case R.id.action_search:
                view = LayoutInflater.from(MainActivity.this).inflate(R.layout.search, null);

                alertBuilder = new AlertDialog.Builder(MainActivity.this);
                alertBuilder.setView(view);
                userInput = (EditText) view.findViewById(R.id.userInput);


                alertBuilder.setCancelable(true)
                        .setPositiveButton("Go", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Search starts a new MainActivity with custom made list for adapter
                                user.hasText(userInput.getText().toString());
                                int position = 4;
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                intent.putExtra("position", position);
                                startActivity(intent);
                            }
                        });
                Dialog dialog1 = alertBuilder.create();
                dialog1.show();

                break;
            //Home button goes back to CategoriesActivity
            case R.id.action_home:
                try {
                    finish();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                break;
            //Refresh gets new articles
            case R.id.action_refresh:
                Thread t = new Thread(reader);
                t.start();

                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                user.notifyObservers();
                break;
        }

        return false;
    }

    //The button sorts articles by publish date
    public void equalClicked(View v) throws IOException {
        //Thread t = new Thread(reader);

        user.sortList(user.getAllEntries());
        user.notifyObservers();


    }

    public void newsClicked(View v) throws IOException {

    }

    @Override
    public void feedUpdated() {
        System.out.println("appxxx feedUpdated()");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseAdapter) feedListView.getAdapter()).notifyDataSetChanged();
            }
        });
    }

}


