package com.example.topipenttila.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.net.MalformedURLException;

/**
 * Created by topipenttila on 10/10/15.
 * ReadActivity has all the read articles
 */
public class ReadActivity extends AppCompatActivity {

    User user = User.getInstance();
    private ListView feedListView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);


        feedListView2 = (ListView) findViewById(R.id.feedListView2);

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, user.getRead());
        feedListView2.setAdapter(adapter);

        feedListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //OnItemClick open web view
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Entry e = (Entry) feedListView2.getItemAtPosition(position);
                user.addToRead(e);
                user.removeFromAll(e);

                String url = e.getLink();
                try {
                    Intent intent = new Intent(ReadActivity.this, ArticleActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });

        feedListView2.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            //OnItemLongClick removes the article from read articles
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                           long id) {

                Entry e = (Entry) feedListView2.getItemAtPosition(position);

                user.removeFavorite(e);
                Context context = getApplicationContext();
                CharSequence text = "Feed " + e.getTitle() + " removed from read articles";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                return true;
            }
        });
    }


}

