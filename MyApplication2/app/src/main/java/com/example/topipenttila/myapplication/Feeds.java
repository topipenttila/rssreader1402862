package com.example.topipenttila.myapplication;

import android.app.Activity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by topipenttila on 03/10/15.
 */
public class Feeds extends Activity implements Serializable {

    private ArrayList<Entry> list;

    public Feeds() {
        this.list = new ArrayList<>();
    }

    public void addEntry(Entry e) {
        this.list.add(e);
    }

    public ArrayList getEntries() {
        return this.list;

    }
}


