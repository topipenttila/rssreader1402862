package com.example.topipenttila.myapplication;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FeedUpdateObserver {
    private FeedReader reader;
    private ListView feedListView;
    User user = User.getInstance();
    Feeds feeds = User.getInstance().getFeeds();

    DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    String mTitle= "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = (String) getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, /*R.drawable.ic_drawer,*/ R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Select a category");
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        ArrayAdapter<String> drawAdapter = new ArrayAdapter<String>(
                getBaseContext(), R.layout.drawer_list_view, getResources().getStringArray(R.array.categories)
        );
        mDrawerList.setAdapter(drawAdapter);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String [] categories = getResources().getStringArray(R.array.categories);
                mTitle = categories[position];
                DrawerFragment cFragment = new DrawerFragment();
                Bundle data = new Bundle();
                data.putInt("position", position);
                cFragment.setArguments(data);
                android.app.FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content_frame,cFragment);
                ft.commit();
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        feedListView = (ListView) findViewById(R.id.feedListView);
        user.registerObserver(this);

        User.getInstance().registerObserver(this);

        try {
            reader = new FeedReader(position);
            System.out.println("-------" +position);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, user.getEntries());
        feedListView.setAdapter(adapter);

        feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Entry e = (Entry) feedListView.getItemAtPosition(position);

                String url = e.getLink();
                try {
                    Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                } catch(Exception ex) {
                    System.out.println(ex);
                }
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
    public void equalClicked(View v) throws IOException {
                Thread t = new Thread(reader);
                t.start();
                user.sortList();
                user.notifyObservers();


                //((BaseAdapter)feedListView.getAdapter()).notifyDataSetChanged();



    }
    public void newsClicked(View v) throws IOException {

    }

    @Override
    public void feedUpdated() {
        System.out.println("appxxx feedUpdated()");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //((BaseAdapter)feedListView.getAdapter()).notifyDataSetChanged();
            }
        });
    }

}


