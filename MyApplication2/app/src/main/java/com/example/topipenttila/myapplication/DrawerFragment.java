package com.example.topipenttila.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by topipenttila on 07/10/15.
 */
public class DrawerFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int position = getArguments().getInt("position");
        String[] categories = getResources().getStringArray(R.array.categories);
        View v = inflater.inflate(R.layout.fragment_layout, container, false);
        TextView tv = (TextView) v.findViewById(R.id.tv_content);
        tv.setText(categories[position]);
        getActivity().getActionBar().setTitle(categories[position]);
        return v;

    }
}
