package com.example.topipenttila.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by topipenttila on 03/10/15.
 */
public class ArticleActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_web_view);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");

        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.loadUrl(url);
    }
}
