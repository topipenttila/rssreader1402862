package com.example.topipenttila.myapplication;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by topipenttila on 29/09/15.
 */
public class Entry implements Serializable {

    private String title;
    private String description;
    private long pubDate;
    private String link;

    public Entry(String title, String description, long pubDate, String link) {
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.link = link;

    }
    public String getLink() {
        return this.link;
    }

    public String toString() {
        return "---" + this.pubDate + "\n " + this.title + "\n";
    }
    public long getPubDate() {
        return this.pubDate;
    }


}