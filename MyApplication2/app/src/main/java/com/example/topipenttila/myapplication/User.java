package com.example.topipenttila.myapplication;

import android.graphics.Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by topipenttila on 01/10/15.
 */
public class User {
    private static final User INSTANCE = new User();
    private ArrayList<Entry> list;
    private Feeds feeds = new Feeds();

    private ArrayList<FeedUpdateObserver> observers;

    private User() {
        this.list = new ArrayList<>();
        this.observers = new ArrayList<>();
    }

    public void registerObserver(FeedUpdateObserver o) {
        observers.add(o);
    }

    public void deregisterObserver(FeedUpdateObserver o) {

    }

    public void notifyObservers() {
        for (FeedUpdateObserver o : observers)
            o.feedUpdated();
    }

    public Feeds getFeeds() {
        return feeds;
    }

    public void addEntry(Entry e) {
        list.add(e);
    }

    public ArrayList getEntries() {
        return this.list;
    }

    public static User getInstance() {
        return INSTANCE;
    }

    public void sortList() {
        Collections.sort(list, new Comparator<Entry>() {
            @Override
            public int compare(Entry rss1, Entry rss2) {
                if (rss1.getPubDate() < rss2.getPubDate())
                    return 1;
                if (rss1.getPubDate() > rss2.getPubDate())
                    return -1;
                return 0;
            }
        });
    }
}
