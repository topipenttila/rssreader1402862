package com.example.topipenttila.myapplication;

import android.app.Activity;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by topipenttila on 29/09/15.
 */
public class FeedReader extends Activity implements Runnable {
    User user = User.getInstance();
    Feeds feeds = User.getInstance().getFeeds();
    ArrayList<String> channelTitles = new ArrayList<>();

    String title = "";
    String description = "";
    String pubDate = "";
    String channelTitle = "";
    String link = "";
    int position;
    long timeInMinutesSinceEpoch;

    ArrayList<URL> urls;

    public FeedReader() throws MalformedURLException {
        this.position = 0;
        urls = new ArrayList<>();
        urls.add(new URL("http://yle.fi/uutiset/rss/paauutiset.rss"));
        urls.add(new URL("http://www.iltalehti.fi/rss/rss.xml"));
        channelTitles.add("All");
    }

    public FeedReader(int position) throws MalformedURLException {
        this.position = position;
        urls = new ArrayList<>();
        urls.add(new URL("http://yle.fi/uutiset/rss/paauutiset.rss"));
        urls.add(new URL("http://www.iltalehti.fi/rss/rss.xml"));
    }

    @Override
    public void run() {
        if (this.position == 0) {
            for (URL url : urls) {
                try {
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();
                    xpp.setInput(new BufferedReader(new InputStreamReader(urlConnection.getInputStream())));

                    boolean insideItem = false;

                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {

                        if (eventType == XmlPullParser.START_DOCUMENT) {

                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("item")) {
                                insideItem = true;
                            } else if (xpp.getName().equalsIgnoreCase("title")) {
                                if (insideItem) {
                                    title = xpp.nextText(); //extract the title
                                } else {
                                    channelTitle = xpp.nextText();
                                    channelTitles.add(channelTitle);
                                }
                            } else if (xpp.getName().equalsIgnoreCase("link")) {
                                if (insideItem) {
                                    link = xpp.nextText(); //extract the title
                                }
                            } else if (xpp.getName().equalsIgnoreCase("description")) {
                                if (insideItem) {
                                    description = xpp.nextText(); //extract the title
                                }
                            } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                                if (insideItem) {
                                    pubDate = xpp.nextText(); //extract the title
                                    SimpleDateFormat sdf  = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
                                    Date date = sdf.parse(pubDate);
                                    long timeInMillisSinceEpoch = date.getTime();
                                    timeInMinutesSinceEpoch = timeInMillisSinceEpoch / (60 * 1000);
                                }

                            }
                        } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                            Entry entry = new Entry(title, description, timeInMinutesSinceEpoch, link);
                            user.addEntry(entry);
                            insideItem = false;

                        } else if (eventType == XmlPullParser.END_TAG) {

                        } else if (eventType == XmlPullParser.TEXT) {

                        }

                        eventType = xpp.next();

                    }
                } catch (Exception e) {
                    System.out.println(e);

                }
               // User.getInstance().notifyObservers();
            }
        } else {

            try {
                HttpURLConnection urlConnection = (HttpURLConnection) urls.get(position).openConnection();
                System.out.println("-----elsessä" + position + urls.get(position));
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(new BufferedReader(new InputStreamReader(urlConnection.getInputStream())));

                boolean insideItem = false;

                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {

                    if (eventType == XmlPullParser.START_DOCUMENT) {

                    } else if (eventType == XmlPullParser.START_TAG) {
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        } else if (xpp.getName().equalsIgnoreCase("title")) {
                            if (insideItem) {
                                title = xpp.nextText(); //extract the title
                            } else {
                                channelTitle = xpp.nextText();
                                channelTitles.add(channelTitle);
                                System.out.println(channelTitle);
                            }
                        } else if (xpp.getName().equalsIgnoreCase("link")) {
                            if (insideItem) {
                                link = xpp.nextText(); //extract the title
                            }
                        } else if (xpp.getName().equalsIgnoreCase("description")) {
                            if (insideItem) {
                                description = xpp.nextText(); //extract the title
                            }
                        } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                            if (insideItem) {
                                pubDate = xpp.nextText(); //extract the title
                                SimpleDateFormat sdf  = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
                                Date date = sdf.parse(pubDate);
                                long timeInMillisSinceEpoch = date.getTime();
                                timeInMinutesSinceEpoch = timeInMillisSinceEpoch / (60 * 1000);
                            }

                        }
                    } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        Entry entry = new Entry(title, description, timeInMinutesSinceEpoch, link);
                        user.addEntry(entry);
                        System.out.println(entry.toString());
                        insideItem = false;

                    } else if (eventType == XmlPullParser.END_TAG) {

                    } else if (eventType == XmlPullParser.TEXT) {

                    }

                    eventType = xpp.next();

                }
            } catch (Exception e) {
                System.out.println(e);

            }

            User.getInstance().notifyObservers();

        }
    }


    public String getTile() {
        return this.title;
    }
    public String getDescription() {
        return this.description;
    }
    public String getPubDate() {
        return this.pubDate;
    }
    public ArrayList<String> getChannelTitles() {
        return this.channelTitles;
    }

    public ArrayList<URL> getUrls() {
        return urls;
    }

}
